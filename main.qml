import QtQuick 2.7
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.3

ApplicationWindow {
	visible: true
	width: 640
	height: 480
	title: qsTr("Hello World")

	MouseArea {
		id: mouseArea
		anchors.fill: parent
		acceptedButtons: Qt.LeftButton | Qt.RightButton
		onClicked: {
			if (mouse.button === Qt.RightButton)
				parent.color = 'blue';
			else
			{
				var component = Qt.createComponent("Circle.qml");
				var circle = component.createObject(parent, {"x": mouse.x - 5, "y": mouse.y - 5})
			}

		}
	}
}
